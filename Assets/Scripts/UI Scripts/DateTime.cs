﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DateTime : MonoBehaviour
{
	//The TMPro that will display, defined in inspector
	public TextMeshProUGUI timeT;
	public TextMeshProUGUI dateD;

	// Update is called once per frame
	void Update()
	{
		//Creates string that stores systems time and date
		string timeS = System.DateTime.UtcNow.ToLocalTime().ToString("HH:mm");
		string dateS = System.DateTime.UtcNow.ToLocalTime().ToString("dd-MM-yy");
		
		//Sets the text to the systems time
		timeT.text = timeS;
		dateD.text = dateS;
	}
}
