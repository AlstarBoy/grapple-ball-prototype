﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Ballcontroller : NetworkBehaviour
{
    
    [SyncVar]
    public float heldFor=0;
    [SyncVar]
    public bool isHeld = false;
    [SyncVar]
    public GameObject heldBy=null;
    private Vector3 offset = new Vector3(0, 1, 0);

    //while held variables
    [SerializeField]
    private Collider sphererCol;
    private Rigidbody rb;
    private PlayerBallController ballcont;
    [SyncVar]
    private float delay;
    [SyncVar]
    private NetworkIdentity networkid;
    
    // Start is called before the first frame update

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame

    private void FixedUpdate()
    {
        
        if (!isHeld)
        {
            delay += Time.deltaTime;
            heldFor = 0;
            WhileNotHeld();
        }
        else
        {
            heldFor += Time.deltaTime;
            delay = 0;
            WhileHeld();
        }

        //Debug.Log(rb.velocity);

    }

    public void Pickup(GameObject holder,NetworkIdentity nett)
    {
        if (delay > 0.3)
        {
            if (!isHeld)
            {
                networkid = nett;
                sphererCol.enabled = false;
                transform.position = holder.transform.position + offset;
                
                heldBy = holder;
               // ballcont = heldBy.GetComponent<PlayerBallController>();
               // ballcont.pickupp(netIdentity);
                isHeld = true;
                
            }
          
            else if (heldFor > 1)
            {
                
                ballcont.droppp(netIdentity);
                sphererCol.enabled = false;
                transform.position = holder.transform.position + offset;
                
                heldBy = holder;
               // ballcont = heldBy.GetComponent<PlayerBallController>();
               // ballcont.pickupp(netIdentity);
                isHeld = true;
                networkid = nett;


            }
        }
        
        
    }
    private void WhileHeld()
    {
        sphererCol.enabled = false;
        transform.position = heldBy.transform.position + offset;
        Vector3 tempvel = heldBy.GetComponent<Rigidbody>().velocity;
        rb.velocity = tempvel;

    }
    private void WhileNotHeld()
    {
      
            sphererCol.enabled = true;
        
      
        
        
    }
    //[Command]
    public void Drop(GameObject droper)
    {
        if (isHeld)
        {

           // ballcont.droppp(netIdentity);
            isHeld = false;
            heldBy = null;
            transform.position = transform.position + (Vector3.up * 1.5f);
            rb.velocity = heldBy.GetComponent<Rigidbody>().velocity;

        }

    }
    [Command(ignoreAuthority =true)]
    public void Throw(Vector3 direction, Vector3 pos,NetworkIdentity NetworkConn)
    {
        if (isHeld && (networkid == NetworkConn))
        {
            networkid = null;
            //direction = new Vector3(0, 10, 0);           
           
            Vector3 tempvel = heldBy.GetComponent<Rigidbody>().velocity;
           // ballcont = heldBy.GetComponent<PlayerBallController>();
            isHeld = false;
            heldBy = null;
            // ballcont.droppp(netIdentity);
            Debug.Log(NetworkConn);

            Debug.DrawLine(pos, pos + (direction.normalized * 2.5f), new Color(0, 0, 0, 1), 10000);
            transform.position = pos + (direction.normalized * 2.5f);
            rb.velocity = tempvel;
            rb.AddForce(direction.normalized * 25, ForceMode.Impulse);
           
          
        }

    }

}
