﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour {

	//Bool that sets the game state
	public static bool GameIsPaused = false;

	//Defines the objects to be used in the script, this is done in the inspector
	public GameObject pauseMenuUI;
	public GameObject playerHUD;

	//Even before start, it sets the cursor invisible and time scale to 1f (allows time to pass)
	void Awake()
	{
		Time.timeScale = 1f;
		Cursor.visible = false;
	}

	// Update is called once per frame
	void Update () 
	{
		//When Escape is pressed
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			//Checks if the game is paused and runs the resume function, also locking the cursor and hiding it
			if (GameIsPaused)
			{
				Resume();
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
			}
			//Runs the pause function, also unlocking the cursor and makes it visisble 
			else
			{
				Pause();
				Cursor.lockState = CursorLockMode.None;
				Cursor.visible = true;
			}
		}
	}

	//The Resume function continues the game and sets the respective values, also resumes time
	public void Resume()
	{
		pauseMenuUI.SetActive(false);
		playerHUD.SetActive(true);
		Time.timeScale = 1f;
		GameIsPaused = false;
		Cursor.lockState = CursorLockMode.Locked;
	}

	//The Pause funtion stops the game running and show the pause menu to the player
	void Pause()
	{
		pauseMenuUI.SetActive(true);
		playerHUD.SetActive(false);
		Time.timeScale = 0f;
		GameIsPaused = true;
		Cursor.lockState = CursorLockMode.None;
	}
}
