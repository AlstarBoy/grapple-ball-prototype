﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
public class PlayerBallController : NetworkBehaviour
{
    private Ballcontroller ballControl;
    private GameObject ball;
    public LayerMask ballLayer;
    private PlayerMotor playerMotor;
    // Start is called before the first frame update
    void Start()
    {
        ball = GameObject.FindGameObjectWithTag("Ball");
        ballControl = ball.GetComponent<Ballcontroller>();
        playerMotor = this.GetComponent<PlayerMotor>();
        //ballControl.Pickup(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        input();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("ball"))
        {
            var bob = GetComponent<NetworkIdentity>();
            ballControl.Pickup(this.gameObject,bob);
        }
    }
    
    
    private void input()
    {
        if (hasAuthority)
        {
            if (Input.GetKeyDown("v"))
            {

                ballControl.Drop(this.gameObject);



            }
            if (Input.GetKeyDown("b"))
            {
                var bob = GetComponent<NetworkIdentity>();
                ballControl.Throw(playerMotor.cam_forward, transform.position,bob);
                



            }
        }
        
        
    }
    
    public void pickupp(NetworkIdentity item)
    {
        item.AssignClientAuthority(connectionToClient);
    }
  
    public void droppp(NetworkIdentity item)
    {
        item.RemoveClientAuthority();
    }
}
