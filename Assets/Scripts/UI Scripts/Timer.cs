﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{
    //Setting variables to be used in the script
    private bool Count = false;
    private float Time = 0f;

    //Defines the objects to be used in the script, this is done in the inspector
    public TextMeshProUGUI timer;

    // Update is called once per frame
    void Update()
    {
        //Creates string that stores the timer
        string timerX = ("Time: ") + Time.ToString();

        //Sets the text to the timerX
        timer.text = timerX;
    }

    //If count is true the timer will begin counting every second
    void FixedUpdate()
    {
        if (Count == true)
        {
            Time += UnityEngine.Time.fixedDeltaTime;
        }
    }

    //if the player collides with an object with these tags it will reset and start the timer or pause the timer
    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "StartGate")
        {
            Time = 0;
            Count = true;
        }
        if (other.gameObject.tag == "EndGate")
        {
            Count = false;
        }
    }
}
