﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class LevelLoader : MonoBehaviour {

    //Defines the objects to be used in the script, this is done in the inspector
    public GameObject loadingScreen;
    public Slider slider;
    public TextMeshProUGUI progressText;

    public void LoadLevel (int sceneIndex)
    {
        //Runs operation
        StartCoroutine(LoadAsynchronously(sceneIndex));
    }

    IEnumerator LoadAsynchronously (int sceneIndex)
    {
        //Async meanns at the same time as other actions rather than pausing everything to load, AsyncOperations makes it a variable and allows us to access and store informatiton
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        
        //Sets the loading screen as active
        loadingScreen.SetActive(true);

        //! makes it while its false, will run untile scene is loaded
        while (!operation.isDone)
        {
            //In unity loading stops at .9 this makes it stop at one
            float progress = Mathf.Clamp01(operation.progress / .9f);

            //This will update the slider with the progress
            slider.value = progress;

            //Add percentage text to loading screen
            progressText.text = progress * 100f + "%";

            //Waits a frame to run the operation again
            yield return null;
        }
    }

    //Quits the application
    public void QuitGame()
    {
        Application.Quit();
    }


}
